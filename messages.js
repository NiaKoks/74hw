const express = require('express');
const router = express.Router();

const fs = require('fs');
const filename = './messages/';

router.get('/',(req,res)=>{
    const messageList = fs.readdirSync(filename);
    const messageArray = messageList.map(file => {
        return JSON.parse(fs.readFileSync(filename + file));
    });
    res.send(messageArray.splice(messageArray.length - 5));
});

router.post('/',(req,res)=>{
    const date = new Date().toISOString();
    const message = {
        message: req.body.message,
        date: date
    };
    fs.writeFileSync(`${filename}${date}.txt`, JSON.stringify(message));
    res.send(message)
});


module.exports = route