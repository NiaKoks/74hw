const express = require('express');
const messages = require('./test');

const app = express();
app.use(express.json());

const port = 8001;

app.use('/messages',messages);

app.listen(port,()=>{
    console.log(`Server started on ${port} port`);
})